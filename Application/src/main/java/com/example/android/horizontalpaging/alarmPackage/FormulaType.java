package com.example.android.horizontalpaging.alarmPackage;

/**
 * Created by denis on 26/05/15.
 */
public enum FormulaType {
    LAST_TRADE, EXP_MOVING_AVERAGE, LIN_MOVING_AVERAGE, CORRELATION
}
