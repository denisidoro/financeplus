package com.example.android.horizontalpaging.utilsPackage;

import java.util.ArrayList;

/**
 * Created by denis on 27/05/15.
 */
public class Utils {

    public static String implode(String glue, ArrayList<String> strArray) {
        String ret = "";
        for (int i = 0; i < strArray.size(); i++) {
            ret += (i == strArray.size() - 1) ? strArray.get(i) : strArray.get(i) + glue;
        }
        return ret;
    }

}
