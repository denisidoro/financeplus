package com.example.android.horizontalpaging.yqlPackage;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import com.example.android.horizontalpaging.stockPackage.Stock;
import com.example.android.horizontalpaging.uiPackage.AssetsListFragment.FragmentCallback;

/**
 * Created by denis on 18/05/15.
 */


public class StockBaseTask extends AsyncTask<String, Stock, Void> {

    private FragmentCallback callback;
    private static final String ns = null;

    String yahooURLFirst = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quote%20where%20symbol%20in%20(%22";
    String yahooURLSecond = "%22)&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

    public StockBaseTask(FragmentCallback callback) {
        this.callback = callback;
    }

    @Override
    protected Void doInBackground(String... strings) {

        try {
            Log.d("yqlm", "In Base XmlPullParser");
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser parser = factory.newPullParser();
            parser.setInput(new InputStreamReader(getUrlData(getUrl(strings[0]))));
            readQuote(parser);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }

        return null;

    }

    public String getUrl(String symbol) {
        return yahooURLFirst + symbol + yahooURLSecond;
    }

    public InputStream getUrlData(String url) throws URISyntaxException,
            ClientProtocolException, IOException {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpGet method = new HttpGet(new URI(url));
        HttpResponse res = client.execute(method);
        //Log.d("yqlm", url);
        return res.getEntity().getContent();
    }

    @Override
    protected void onProgressUpdate(Stock... values) {
        //Log.d("yqlm", "progressUpdate: " + values[0].getName());
        callback.onTaskDone(values[0]);
    }

    private Void readQuote(XmlPullParser parser) throws XmlPullParserException, IOException {

        String symbol = "";
        String name = "";
        float change = 0;
        float lastTrade = 0;

        //Log.d("yqlm", "inside readQuote");

        int eventType = parser.getEventType();

        while (eventType != XmlPullParser.END_DOCUMENT){
            String n = null;
            switch (eventType){
                case XmlPullParser.START_DOCUMENT:
                    break;
                case XmlPullParser.START_TAG:
                    n = parser.getName();
                    //Log.d("yqlm", "start_tag, n: " + n);
                    if (n.equalsIgnoreCase("Name"))
                        name = parser.nextText();
                    else if (n.equalsIgnoreCase("Symbol"))
                        symbol = parser.nextText();
                    else if (n.equalsIgnoreCase("Change"))
                        change = Float.parseFloat(parser.nextText());
                    else if (n.equalsIgnoreCase("LastTradePriceOnly"))
                        lastTrade = Float.parseFloat(parser.nextText());
                    break;
                case XmlPullParser.END_TAG:
                    n = parser.getName();
                    //Log.d("yqlm", "end_tag, n: " + n);
                    if (n.equalsIgnoreCase("quote"))
                        publishProgress(new Stock(symbol, name, change, lastTrade));
                    break;
            }
            eventType = parser.next();
        }

        return null;

    }





}


