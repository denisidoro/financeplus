package com.example.android.horizontalpaging.uiPackage;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.android.horizontalpaging.MainActivity;
import com.example.android.horizontalpaging.R;
import com.example.android.horizontalpaging.alarmPackage.Alarm;
import com.example.android.horizontalpaging.stockPackage.Stock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pedrorychter on 27/05/2015.
 */
public class NewDialogFragmentMovingAverage extends android.support.v4.app.DialogFragment{

    Stock stock;

    public interface DialogClickListener {
        public void onYesClick(DialogFragment dialog);
        public void onNoClick(DialogFragment dialog);
    }

    // Use this instance of the interface to deliver action events
    DialogClickListener mListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogLayout = inflater.inflate(R.layout.dialogfragment_moving_average, null);

        ArrayList<String> items1 = new ArrayList<>();
        List<String> items2 = Arrays.asList("Exponential","Linear");
        List<String> items3 = Arrays.asList("1st moving average < 2nd moving average",
                "1st moving average <= 2nd moving average",
                "1st moving average = 2nd moving average",
                "1st moving average >= 2nd moving average",
                "1st moving average > 2nd moving average");

        for (Map.Entry<String, Stock> e : ((MainActivity)getActivity()).stocks.entrySet()) {
            items1.add(e.getKey());
        }

        Spinner dropdown1 = (Spinner)dialogLayout.findViewById(R.id.spinner1);
        Spinner dropdown2 = (Spinner)dialogLayout.findViewById(R.id.spinner2);
        Spinner dropdown3 = (Spinner)dialogLayout.findViewById(R.id.spinner3);

        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_item, items1);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_item, items2);
        ArrayAdapter<String> adapter3 = new ArrayAdapter<>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_item, items3);

        dropdown1.setAdapter(adapter1);
        dropdown2.setAdapter(adapter2);
        dropdown3.setAdapter(adapter3);

        builder.setView(dialogLayout)
                .setMessage("Hi")
                .setTitle("Add Moving Averages Alarm")
                .setPositiveButton("ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                int f = (int) ((Spinner)dialogLayout.findViewById(R.id.spinner2)).getSelectedItemId() + 1;
                                int i = (int) ((Spinner)dialogLayout.findViewById(R.id.spinner3)).getSelectedItemId();
                                String s = (String) ((Spinner)dialogLayout.findViewById(R.id.spinner1)).getSelectedItem();
                                int p1 = Integer.parseInt((((EditText) dialogLayout.findViewById(R.id.editText5)).getText().toString()));
                                int p2 = Integer.parseInt((((EditText)dialogLayout.findViewById(R.id.editText6)).getText().toString()));

                                Alarm myalarm = new Alarm(s,f,i,p1,p2);
                                myalarm.parseWrite();

                                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, getActivity().getIntent());
                            }
                        }
                )
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_CANCELED, getActivity().getIntent());
                    }
                });

        return builder.create();
    }

    static NewDialogFragmentMovingAverage newInstance() {
        NewDialogFragmentMovingAverage dialogFragment = new NewDialogFragmentMovingAverage();
        return dialogFragment;
    }


}
