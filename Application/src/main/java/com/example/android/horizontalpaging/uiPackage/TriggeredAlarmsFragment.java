package com.example.android.horizontalpaging.uiPackage;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.android.horizontalpaging.MainActivity;
import com.example.android.horizontalpaging.R;
import com.example.android.horizontalpaging.alarmPackage.Alarm;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TriggeredAlarmsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TriggeredAlarmsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TriggeredAlarmsFragment extends android.support.v4.app.Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    //public static final String ARG_SECTION_NUMBER = "section_number";

    //public DummySectionFragment() {
    //}

    private ArrayAdapter<Alarm> triggeredAlarmsAdapter;
    private ListView lvAlarmsTriggered;
    private Alarm currentAlarm;
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ArrayList<Alarm> triggeredAlarmsArray = ((MainActivity)getActivity()).alarms;

        triggeredAlarmsArray.add(new Alarm("AAPL", "YHOO", 3, 0, 95, 20));
        triggeredAlarmsArray.add(new Alarm("YHOO", "AAPL", 3, 4, 99, 10));

        triggeredAlarmsArray.add(new Alarm("AAPL", 1, 0, 10, 20));
        triggeredAlarmsArray.add(new Alarm("AAPL", 1, 1, 30, 40));
        triggeredAlarmsArray.add(new Alarm("AAPL", 1, 2, 50, 60));
        triggeredAlarmsArray.add(new Alarm("AAPL", 2, 3, 70, 80));
        triggeredAlarmsArray.add(new Alarm("AAPL", 2, 4, 90, 100));


        rootView = inflater.inflate(R.layout.fragment_triggered_alarms, container, false);
        lvAlarmsTriggered = (ListView) rootView.findViewById(R.id.lvAlarmsTriggered);
        triggeredAlarmsAdapter = new TriggeredAlarmAdapter(getActivity().getApplicationContext(), triggeredAlarmsArray);
        lvAlarmsTriggered.setAdapter(triggeredAlarmsAdapter);
        config();

        return rootView;

    }

    private void config() {

        // listview item click listener
        lvAlarmsTriggered.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Alarm alarm = ((MainActivity)getActivity()).alarms.get(position);
                //showDialogAlarm(alarm);
            }
        });

    }


}