package com.example.android.horizontalpaging.uiPackage;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.android.horizontalpaging.MainActivity;
import com.example.android.horizontalpaging.uiPackage.MyDialogStock;
import com.example.android.horizontalpaging.R;
import com.example.android.horizontalpaging.alarmPackage.Alarm;
import com.example.android.horizontalpaging.stockPackage.Stock;
import com.example.android.horizontalpaging.utilsPackage.Utils;
import com.example.android.horizontalpaging.yqlPackage.StockBaseTask;
import com.example.android.horizontalpaging.yqlPackage.StockHistoryTask;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AssetsListFragment extends android.support.v4.app.Fragment {

    private ArrayAdapter<Stock> stocksAdapter;
    private ListView lvAssets;
    private Stock currentStock;
    private View rootView;
    public static final int DIALOG_FRAGMENT = 1;
    public static final int HISTORY_DAYS = 50;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ArrayList<Stock> stocksArray = new ArrayList<>();
        for (Map.Entry<String, Stock> e : ((MainActivity)getActivity()).stocks.entrySet()) {
            String symbol = e.getKey();
            Stock stock = e.getValue();
            stocksArray.add(stock);
        }

        rootView = inflater.inflate(R.layout.fragment_assets_list, container, false);
        lvAssets = (ListView) rootView.findViewById(R.id.lvAssets);
        stocksAdapter = new StockAdapter(getActivity().getApplicationContext(), stocksArray);
        lvAssets.setAdapter(stocksAdapter);

        if (stocksArray.size() == 0) {

            ParseQuery<ParseObject> query = ParseQuery.getQuery("Stock");
            query.whereNotEqualTo("symbol", "");

            query.findInBackground(new FindCallback<ParseObject>() {

                public void done(List<ParseObject> symbolExecute, ParseException e) {

                    if (e == null) {

                        ArrayList<String> symbols = new ArrayList<>();
                        Log.d("symbol", "Retrieved " + symbolExecute.size() + " symbols");
                        for (ParseObject obj : symbolExecute) {
                            String symbol = obj.getString("symbol");
                            if (!symbols.contains(symbol))
                                symbols.add(symbol);
                        }

                        StockBaseTask task = new StockBaseTask(new FragmentCallback() {
                            @Override
                            public void onTaskDone(Stock stock) {
                                Log.d("yql", "adding new stock: " + stock.getName() + ", " + stock.getSymbol());
                                addToAdapter(stock);
                                setHistory(stock);
                            }
                        });

                        //task.execute(Utils.implode(",", symbols));
                        task.execute("YHOO,AAPL"); // quicker testing

                    } else {
                        Log.d("symbol", "Error: " + e.getMessage());
                    }
                }
            });

        }

        config();
        return rootView;

    }

    private void config() {

        // listview item click listener
        lvAssets.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String symbol = stocksAdapter.getItem(position).getSymbol();
                Stock stock = ((MainActivity)getActivity()).stocks.get(symbol);
                showDialogStock(stock);
            }
        });

        lvAssets.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                // Remove the item within array at position

                stocksAdapter.getItem(position).deleteEventually();
                stocksAdapter.getItem(position).deleteInBackground();
                ParseQuery<ParseObject> query = ParseQuery.getQuery("Stock");
                query.whereEqualTo("symbol",stocksAdapter.getItem(position).getSymbol());
                query.getFirstInBackground(new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                        try {
                            parseObject.delete();
                        } catch (ParseException e1) {
                            e1.printStackTrace();
                        }
                        parseObject.saveInBackground();
                    }
                });

                stocksAdapter.remove(stocksAdapter.getItem(position));
                // Refresh the adapter
                stocksAdapter.notifyDataSetChanged();

                // Return true consumes the long click event (marks it handled)
                return true;
            }
        });

        // search button click listener
        Button btn = (Button) rootView.findViewById(R.id.btnAddItem);
        btn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

            EditText et = (EditText) rootView.findViewById(R.id.etNewItem);
            String query = et.getText().toString();

            StockBaseTask task = new StockBaseTask(new FragmentCallback() {
                @Override
                public void onTaskDone(Stock stock) {
                    //Log.d("button", "found new info: " + stock.getName());
                    showDialog(stock);
                }
            });

            task.execute(query);

            }

        });

    }

    // callback to communicate the asynctask with the fragment
    public interface FragmentCallback {
        public void onTaskDone(Stock stock);
    }

    private void showDialog(Stock stock) {
        currentStock = stock;
        MyDialogFragment dialog = new MyDialogFragment().newInstance(stock);
        dialog.setTargetFragment(this, DIALOG_FRAGMENT);
        dialog.show(getFragmentManager(), "dialog");
    }

    private void showDialogStock(Stock stock) {
        currentStock = stock;
        MyDialogStock dialog = new MyDialogStock().newInstance(stock);
        dialog.setTargetFragment(this, DIALOG_FRAGMENT);
        dialog.show(getFragmentManager(), "dialog");
    }

    private void addToAdapter(Stock stock) {
        stocksAdapter.add(stock);
        ((MainActivity)getActivity()).stocks.put(stock.getSymbol(), stock);
    }

    private void setHistory(Stock stock) {

        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
        Date now = new Date();
        Date start = new Date(now.getTime() - 1000*60*60*24 * HISTORY_DAYS);

        new StockHistoryTask(new FragmentCallback() {
            @Override
            public void onTaskDone(Stock stock) {
            Log.d("yql", "finished getting history");
            ((MainActivity)getActivity()).stocks.get(stock.getSymbol()).setHistory(stock.getHistory());
            }
        }).execute(stock.getSymbol(), sdfDate.format(start), sdfDate.format(now));

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case DIALOG_FRAGMENT:

                if (resultCode == Activity.RESULT_OK) {
                    Log.d("dialog", "ok");

                    addToAdapter(currentStock);

                    Stock stock = new Stock();
                    Alarm alarm = new Alarm();

                    alarm.setStockSymbol1(currentStock.getSymbol());
                    stock.setParseSymbol(currentStock.getSymbol());
                    stock.saveInBackground();
                    alarm.saveInBackground();

                    Toast.makeText(getActivity().getApplicationContext(), "The stock has been added.", Toast.LENGTH_SHORT).show();

                } else if (resultCode == Activity.RESULT_CANCELED){
                    Log.d("dialog", "cancel");
                }

                break;
        }
    }

}
