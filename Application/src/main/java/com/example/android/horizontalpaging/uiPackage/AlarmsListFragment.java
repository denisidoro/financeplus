package com.example.android.horizontalpaging.uiPackage;

import android.app.DialogFragment;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.android.horizontalpaging.MainActivity;
import com.example.android.horizontalpaging.R;
import com.example.android.horizontalpaging.alarmPackage.Alarm;
import com.example.android.horizontalpaging.stockPackage.Stock;
import com.example.android.horizontalpaging.yqlPackage.StockBaseTask;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class AlarmsListFragment extends android.support.v4.app.Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private ArrayAdapter<Alarm> alarmsAdapter;
    private ListView lvAlarms;
    private Alarm currentAlarm;
    private View rootView;
    private ArrayList<Alarm> alarmsArray;
    public static final int DIALOG_FRAGMENT = 2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        alarmsArray = new ArrayList<>();
        for (Alarm alarm : ((MainActivity)getActivity()).alarms)
            alarmsArray.add(alarm);

        rootView = inflater.inflate(R.layout.fragment_alarms_list, container, false);
        lvAlarms = (ListView) rootView.findViewById(R.id.lvAlarms);
        alarmsAdapter = new AlarmAdapter(getActivity().getApplicationContext(), alarmsArray);
        lvAlarms.setAdapter(alarmsAdapter);
        config();

        return rootView;

    }

    private void showDialogMovingAverage() {
        NewDialogFragmentMovingAverage dialog = new NewDialogFragmentMovingAverage().newInstance();
        dialog.setTargetFragment(this, DIALOG_FRAGMENT);
        dialog.show(getFragmentManager(), "dialog");
    }
    private void showDialogCorrelation() {
        NewDialogFragmentCorrelation dialog = new NewDialogFragmentCorrelation().newInstance();
        dialog.setTargetFragment(this, DIALOG_FRAGMENT);
        dialog.show(getFragmentManager(), "dialog");
    }

    private void config() {

//        // listview item click listener
//        lvAlarms.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Alarm alarm = ((MainActivity)getActivity()).alarms.get(position);
//                //showDialogAlarm(alarm);
//            }
//        });

        lvAlarms.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                // Remove the item within array at position

                alarmsAdapter.getItem(position).deleteEventually();
                alarmsAdapter.getItem(position).deleteInBackground();
                ParseQuery<ParseObject> query = ParseQuery.getQuery("Alarm");
                query.whereEqualTo("stockSymbol1", alarmsAdapter.getItem(position).getStockSymbol1());
                query.getFirstInBackground(new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                        try {
                            parseObject.delete();
                        } catch (ParseException e1) {
                            e1.printStackTrace();
                        }
                        parseObject.saveInBackground();
                    }
                });

                alarmsAdapter.remove(alarmsAdapter.getItem(position));
                // Refresh the adapter
                alarmsAdapter.notifyDataSetChanged();

                // Return true consumes the long click event (marks it handled)
                return true;
            }
        });

        // search button click listener
        Button btnMovingAverage = (Button) rootView.findViewById(R.id.btnMovingAverage);
        btnMovingAverage.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                showDialogMovingAverage();
            }

        });

        Button btnCorrelation = (Button) rootView.findViewById(R.id.btnCorrelation);
        btnCorrelation.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                showDialogCorrelation();
            }

        });



        ParseQuery<ParseObject> query = ParseQuery.getQuery("Alarm");
        query.whereNotEqualTo("symbolStock1", "");

        query.findInBackground(new FindCallback<ParseObject>() {

            public void done(List<ParseObject> symbolExecute, ParseException e) {

                if (e == null) {

                    if (alarmsArray.size() == 0) {

                        for (ParseObject obj : symbolExecute) {
                            Alarm newAlarm = new Alarm(
                                    obj.getString("stockSymbol1"),
                                    obj.getString("stockSymbol2"),
                                    obj.getInt("formulaTypeID"),
                                    obj.getInt("inequalityID"),
                                    (float) obj.getDouble("threshold"),
                                    obj.getInt("param1"),
                                    obj.getInt("param2")
                            );
                            alarmsAdapter.add(newAlarm);
                            alarmsAdapter.notifyDataSetChanged();
                            ((MainActivity) getActivity()).alarms.add(newAlarm);
                        }
                    }

                }

            }

        });


    }


}
