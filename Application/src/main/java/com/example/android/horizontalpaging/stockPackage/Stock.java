package com.example.android.horizontalpaging.stockPackage;

import android.util.Log;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.ArrayList;


@ParseClassName("Stock")
public class Stock extends ParseObject {

    private String symbol;
    private String name;
    private float change;
    private float lastTrade;
    private ArrayList<HistoryPoint> history;

    public Stock() {}

    public Stock(String s, String n, Float c, Float l) {
        symbol = s;
        name = n;
        change = c;
        lastTrade = l;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getName() {
        return name;
    }

    public float getChange() {
        return change;
    }

    public float getLastTrade() {
        return lastTrade;
    }

    public ArrayList<HistoryPoint> getHistory() {
        return history;
    }

    public void setHistory(ArrayList<HistoryPoint> history) {
        this.history = history;
    }

    public void setSymbol(String symbol) { this.symbol = symbol; }

    public void setParseSymbol(String symbol){put("symbol",symbol);}

    public String getParseSymbol(){return getString("symbol");}



}
