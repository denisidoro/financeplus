package com.example.android.horizontalpaging.stockPackage;

import android.util.Log;

/**
 * Created by denis on 19/05/15.
 */
public class HistoryPoint {

    private String date;
    private float close;

    public HistoryPoint() {}

    public HistoryPoint(String date, float close) {
        this.date = date;
        this.close = close;
    }

    public void update(int id, String value) {
        Log.d("yqlm", id + ", " + value);
        switch (id) {
            case 0:
                this.date = value;
                break;
            case 1:
                this.close = Float.parseFloat(value);
                break;
        }
    }

    public String getDate() {
        return date;
    }

    public float getClose() {
        return close;
    }
}
