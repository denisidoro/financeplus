package com.example.android.horizontalpaging.yqlPackage;

import android.os.AsyncTask;
import android.util.Log;

import com.example.android.horizontalpaging.stockPackage.HistoryPoint;
import com.example.android.horizontalpaging.stockPackage.Stock;
import com.example.android.horizontalpaging.uiPackage.AssetsListFragment;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

/**
 * Created by denis on 19/05/15.
 */
public class StockHistoryTask extends AsyncTask<String, Void, Boolean> {

    private AssetsListFragment.FragmentCallback callback;
    private Stock stock = new Stock();
    private ArrayList<HistoryPoint> history = new ArrayList<>();

    String yahooUrlStart = "http://query.yahooapis.com/v1/public/yql?q=";
    String yahooUrlEnd = "&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

    public StockHistoryTask(AssetsListFragment.FragmentCallback callback) {
        this.callback = callback;
    }

    @Override
    protected Boolean doInBackground(String... strings) {

        try {

            Log.d("test", "In History XmlPullParser");

            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser parser = factory.newPullParser();
            stock.setSymbol(strings[0]);
            parser.setInput(new InputStreamReader(getUrlData(getUrl(strings[0], strings[1], strings[2]))));
            readQuote(parser);

            return true;

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            return false;
        }

    }

    public String getUrl(String symbol, String startDate, String endDate) {

        String query1 = "select%20Open%2C%20Date%2C%20Close%20from%20yahoo.finance.historicaldata%20where%20symbol%20%3D%20%22";
        String query2 = "%22%20and%20startDate%20%3D%20%22";
        String query3 = "%22%20and%20endDate%20%3D%20%22";
        String query4 = "%22";
        String finalUrl = yahooUrlStart + query1 + symbol + query2 + startDate + query3 + endDate + query4 + yahooUrlEnd;
        //Log.d("url", finalUrl);
        return finalUrl;

    }


    private Void readQuote(XmlPullParser parser) throws XmlPullParserException, IOException {

        String date = "";
        float close = 0;

        //Log.d("yqlm", "inside readQuote");

        int eventType = parser.getEventType();

        while (eventType != XmlPullParser.END_DOCUMENT){
            String n = null;
            switch (eventType){
                case XmlPullParser.START_DOCUMENT:
                    break;
                case XmlPullParser.START_TAG:
                    n = parser.getName();
                    //Log.d("yqlm", "start_tag, n: " + n);
                    if (n.equalsIgnoreCase("Date"))
                        date = parser.nextText();
                    else if (n.equalsIgnoreCase("Close"))
                        close = Float.parseFloat(parser.nextText());
                    break;
                case XmlPullParser.END_TAG:
                    n = parser.getName();
                    //Log.d("yqlm", "end_tag, n: " + n);
                    if (n.equalsIgnoreCase("quote"))
                        //Log.d("yqlm", "both found!");
                        history.add(new HistoryPoint(date, close));
                    break;
            }
            eventType = parser.next();
        }

        return null;

    }

    public InputStream getUrlData(String url) throws URISyntaxException,
            ClientProtocolException, IOException {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpGet method = new HttpGet(new URI(url));
        HttpResponse res = client.execute(method);
        Log.d("yql", url);
        return res.getEntity().getContent();
    }

    protected void onPostExecute(Boolean success) {
        stock.setHistory(history);
        callback.onTaskDone(stock);
    }


}


