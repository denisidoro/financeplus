package com.example.android.horizontalpaging.uiPackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.android.horizontalpaging.R;
import com.example.android.horizontalpaging.alarmPackage.Alarm;
import com.example.android.horizontalpaging.stockPackage.Stock;

import java.util.ArrayList;

/**
 * Created by denis on 20/05/15.
 */
public class AlarmAdapter extends ArrayAdapter<Alarm> {

    public AlarmAdapter(Context context, ArrayList<Alarm> alarms) {
        super(context, 0, alarms);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Alarm alarm = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_alarm, parent, false);
        }
        // Lookup view for data population
        TextView tvFormulaType = (TextView) convertView.findViewById(R.id.tvFormulaType);
        TextView tvStock1 = (TextView) convertView.findViewById(R.id.tvStock1);
        TextView tvPeriod1 = (TextView) convertView.findViewById(R.id.tvPeriod1);
        TextView tvStock2 = (TextView) convertView.findViewById(R.id.tvStock2);
        TextView tvPeriod2 = (TextView) convertView.findViewById(R.id.tvPeriod2);
        TextView tvInequalitySignificance = (TextView) convertView.findViewById(R.id.tvInequalitySignificance);
        // Populate the data into the template view using the data object

        int FID;
        FID = alarm.getFormulaTypeId();
        if(FID == 0) {
            tvFormulaType.setText("Last Trade");
        }
        else if (FID == 1) {
            tvFormulaType.setText("Exponential Moving Averages");
        }
        else if (FID == 2) {
            tvFormulaType.setText("Linear Moving Averages");
        }
        else if (FID == 3) {
            tvFormulaType.setText("Simple Correlation");
        }

        int IID;
        IID = alarm.getInequalityId();

        if(alarm.getStockSymbol2()==""){
            tvStock1.setText("First moving average: " + alarm.getStockSymbol1());
            tvStock2.setText("Second moving average : " + alarm.getStockSymbol1());
            tvPeriod1.setText(alarm.getParam1() + " days");
            tvPeriod2.setText(alarm.getParam2() + " days");

            if(IID == 0) {
                tvInequalitySignificance.setText("Warn me if 1st moving average < 2nd moving average");
            }
            else if (IID == 1) {
                tvInequalitySignificance.setText("Warn me if 1st moving average <= 2nd moving average");
            }
            else if (IID == 2) {
                tvInequalitySignificance.setText("Warn me if 1st moving average = 2nd moving average");
            }
            else if (IID == 3) {
                tvInequalitySignificance.setText("Warn me if 1st moving average >= 2nd moving average");
            }
            else if (IID == 4) {
                tvInequalitySignificance.setText("Warn me if 1st moving average > 2nd moving average");
            }
        }
        else {
            if(IID == 0) {
                tvInequalitySignificance.setText("Warn me if a correlation with a significance degree of at least "
                        + Double.toString(alarm.getThreshold()) + " % for the last " + alarm.getParam1()
                        + " days doesn't exist any longer between:");
            }
            else if (IID == 4) {
                tvInequalitySignificance.setText("Warn me if there is a simple correlation with a significance degree of at least "
                        + Double.toString(alarm.getThreshold()) + " % for the last " + alarm.getParam1()
                        + " days between:");
            }

            tvStock1.setText("First asset: " + alarm.getStockSymbol1());
            tvStock2.setText("Second asset: " + alarm.getStockSymbol2());
            tvPeriod1.setText("");
            tvPeriod2.setText("");
        }

        // Return the completed view to render on screen
        return convertView;
    }
}