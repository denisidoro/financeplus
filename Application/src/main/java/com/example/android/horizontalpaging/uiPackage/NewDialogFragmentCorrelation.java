package com.example.android.horizontalpaging.uiPackage;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.android.horizontalpaging.MainActivity;
import com.example.android.horizontalpaging.R;
import com.example.android.horizontalpaging.alarmPackage.Alarm;
import com.example.android.horizontalpaging.stockPackage.Stock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by pedrorychter on 01/06/2015.
 */
public class NewDialogFragmentCorrelation extends android.support.v4.app.DialogFragment{

    Stock stock;

    public interface DialogClickListener {
        public void onYesClick(DialogFragment dialog);
        public void onNoClick(DialogFragment dialog);
    }

    // Use this instance of the interface to deliver action events
    DialogClickListener mListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogLayout = inflater.inflate(R.layout.dialogfragment_correlation, null);

        ArrayList<String> items1 = new ArrayList<>();
        ArrayList<String> items2 = new ArrayList<>();
        List<String> items3 = Arrays.asList("There is no correlation any longer",
                "There is a simple correlation");

        for (Map.Entry<String, Stock> e : ((MainActivity)getActivity()).stocks.entrySet()) {
            items1.add(e.getKey());
        }
        for (Map.Entry<String, Stock> e : ((MainActivity)getActivity()).stocks.entrySet()) {
            items2.add(e.getKey());
        }

        Spinner dropdown1 = (Spinner)dialogLayout.findViewById(R.id.spinner1);
        Spinner dropdown2 = (Spinner)dialogLayout.findViewById(R.id.spinner2);
        Spinner dropdown3 = (Spinner)dialogLayout.findViewById(R.id.spinner3);

        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_item, items1);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_item, items2);
        ArrayAdapter<String> adapter3 = new ArrayAdapter<>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_item, items3);

        dropdown1.setAdapter(adapter1);
        dropdown2.setAdapter(adapter2);
        dropdown3.setAdapter(adapter3);

        builder.setView(dialogLayout)
                .setMessage("Hi")
                .setTitle("Add Correlation Alarm")
                .setPositiveButton("ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                int f = 3;
                                int i = (int) ((Spinner)dialogLayout.findViewById(R.id.spinner3)).getSelectedItemId()*4;
                                String ss1 = (String) ((Spinner)dialogLayout.findViewById(R.id.spinner1)).getSelectedItem();
                                String ss2 = (String) ((Spinner)dialogLayout.findViewById(R.id.spinner2)).getSelectedItem();
                                float t = Integer.parseInt((((EditText) dialogLayout.findViewById(R.id.editText7)).getText().toString()));
                                int p = Integer.parseInt((((EditText) dialogLayout.findViewById(R.id.editText8)).getText().toString()));

                                Alarm myAlarm = new Alarm(ss1,ss2,f,i,t,p);
                                myAlarm.parseWrite();

                                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, getActivity().getIntent());
                            }
                        }
                )
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_CANCELED, getActivity().getIntent());
                    }
                });

        return builder.create();
    }

    static NewDialogFragmentCorrelation newInstance() {
        NewDialogFragmentCorrelation dialogFragment = new NewDialogFragmentCorrelation();
        return dialogFragment;
    }

}
