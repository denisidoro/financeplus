package com.example.android.horizontalpaging.alarmPackage;

import android.util.Log;

import com.example.android.horizontalpaging.MainActivity;
import com.example.android.horizontalpaging.stockPackage.Stock;
import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.HashMap;


@ParseClassName("Alarm")
public class Alarm extends ParseObject {

    private String stockSymbol1;

    private String stockSymbol2 ;
    private int formulaTypeId;
    private int inequalityId;
    private float threshold;
    private int param1;
    private int param2;
    private float value;
    private String date;

    public Alarm(){};

    // WHERE TO PUT param1, param2 in the constructors?????

    public Alarm(String ss1, String ss2, int f, int i, float t, int p1, int p2) {
        stockSymbol1 = ss1;
        stockSymbol2 = ss2;
        formulaTypeId = f;
        inequalityId = i;
        threshold = t;
        param1 = p1;
        param2 = p2;
        date = "";
    }

    public Alarm(String ss1, String ss2, int f, int i, float t, int p1) {
        this(ss1, ss2, f, i, t, p1, 0);
    }

    public Alarm(String ss, int f, int i, int p1, int p2) {
        this(ss, "", f, i, 0, p1, p2);
    }

    public void parseWrite() {
        put("stockSymbol1", stockSymbol1);
        put("stockSymbol2", stockSymbol2);
        put("formulaTypeID", formulaTypeId);
        put("inequalityID", inequalityId);
        put("threshold", threshold);
        put("param1", param1);
        put("param2", param2);
        put("date", date);
    }

    public void calculate(HashMap<String, Stock> stocks) {

        FormulaType formulaType = FormulaType.values()[formulaTypeId];
        Stock stock1 = null;
        Stock stock2 = null;

        if (!stockSymbol1.equalsIgnoreCase(""))
            stock1 = stocks.get(stockSymbol1);
        if (!stockSymbol2.equalsIgnoreCase(""))
            stock2 = stocks.get(stockSymbol2);

        if ((stock1 != null && stock1.getHistory() == null) ||
                (stock2 != null && stock2.getHistory() == null)) {
            Log.d("alarm", "stock not ready");
            return;
        }

        if (stock2 == null) {
            value = Finance.calculate(formulaType, stock1, param1, param2)[0];
            this.threshold = Finance.calculate(formulaType, stock1, param1, param2)[1];
        }
        else {
            value = Finance.calculate(formulaType, stock1, stock2, param1)[1];

            float cum_proba;
            cum_proba = (threshold + (100 - threshold) / 2) / 100;

            this.threshold = (float) new org.apache.commons.math3.distribution.TDistribution(param1 - 2)
                    .inverseCumulativeProbability(cum_proba);
        }

    }

    public boolean check() {

        Inequality inequality = Inequality.values()[inequalityId];
        Log.d("alarm", "now checking: " + value + ", " + threshold);

        switch (inequality) {
            case LESS:
                return value < threshold;
            case LESS_EQUAL:
                return value <= threshold;
            case EQUAL:
                return value == threshold;
            case GREATER_EQUAL:
                return value >= threshold;
            case GREATER:
                return value > threshold;
            default:
                return false;
        }

    }

    public String getStockSymbol1() {
        return stockSymbol1;
    }
    public void setStockSymbol1(String stockSymbol1) {
        this.stockSymbol1 = stockSymbol1;
    }
    public String getStockSymbol2() {
        return stockSymbol2;
    }
    public void setStockSymbol2(String stockSymbol2) {
        this.stockSymbol2 = stockSymbol2;
    }
    public int getFormulaTypeId() {
        return formulaTypeId;
    }
    public void setFormulaTypeId(int formulaTypeId) {
        this.formulaTypeId = formulaTypeId;
    }
    public int getInequalityId() {
        return inequalityId;
    }
    public void setInequalityId(int inequalityId) {
        this.inequalityId = inequalityId;
    }
    public float getThreshold() {
        return threshold;
    }
    public void setThreshold(float threshold) {
        this.threshold = threshold;
    }
    public int getParam1() {
        return param1;
    }
    public void setParam1(int param1) {
        this.param1 = param1;
    }
    public int getParam2() {
        return param2;
    }
    public void setParam2(int param2) {
        this.param2 = param2;
    }
    public String getDate() {
        return date; }
    public void setDate(String date) {
        this.date = date;
    }

}