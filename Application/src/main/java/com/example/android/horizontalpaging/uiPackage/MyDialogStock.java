package com.example.android.horizontalpaging.uiPackage;

import android.app.Activity;
import android.content.Context;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.android.horizontalpaging.R;
import com.example.android.horizontalpaging.stockPackage.Stock;

import junit.framework.Test;



public class MyDialogStock extends android.support.v4.app.DialogFragment {

    Stock stock;

    public interface DialogClickListener {
        public void onYesClick(DialogFragment dialog);
        public void onNoClick(DialogFragment dialog);
    }

    // Use this instance of the interface to deliver action events
    DialogClickListener mListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogLayout = inflater.inflate(R.layout.dialogfragment_stock, null);

        TextView tvSymbol = (TextView) dialogLayout.findViewById(R.id.TextView1);
        TextView tvLastTrade = (TextView) dialogLayout.findViewById(R.id.TextView2);
        TextView tvChange = (TextView) dialogLayout.findViewById(R.id.TextView3);

        tvSymbol.setText(stock.getSymbol());
        tvLastTrade.setText(String.valueOf(stock.getLastTrade()));
        tvChange.setText(String.valueOf(stock.getChange()));

        builder.setView(dialogLayout)
                .setMessage("Company: " + stock.getName())
                .setTitle("Stock Data")
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dismiss();
                            }
                        }
                );

        return builder.create();
    }

    public static MyDialogStock newInstance(Stock stock) {
        MyDialogStock dialogFragment = new MyDialogStock();
        dialogFragment.setStock(stock);
        return dialogFragment;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }


}


