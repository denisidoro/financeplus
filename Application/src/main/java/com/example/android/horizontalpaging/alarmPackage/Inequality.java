package com.example.android.horizontalpaging.alarmPackage;

/**
 * Created by denis on 26/05/15.
 */
public enum Inequality {
    LESS, LESS_EQUAL, EQUAL, GREATER_EQUAL, GREATER
}
