package com.example.android.horizontalpaging.uiPackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.android.horizontalpaging.R;
import com.example.android.horizontalpaging.stockPackage.Stock;

import java.util.ArrayList;

/**
 * Created by denis on 20/05/15.
 */
public class StockAdapter extends ArrayAdapter<Stock> {

    public StockAdapter(Context context, ArrayList<Stock> stocks) {
        super(context, 0, stocks);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Stock stock = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_stock, parent, false);
        }
        // Lookup view for data population
        TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
        TextView tvLastTrade = (TextView) convertView.findViewById(R.id.tvLastTrade);
        // Populate the data into the template view using the data object
        tvName.setText(stock.getSymbol());
        tvLastTrade.setText(Float.toString(stock.getLastTrade()));
        // Return the completed view to render on screen
        return convertView;
    }
}