package com.example.android.horizontalpaging.alarmPackage;

import com.example.android.horizontalpaging.stockPackage.HistoryPoint;
import com.example.android.horizontalpaging.stockPackage.Stock;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by denis on 26/05/15.
 */
public class Finance {

    public static float[] calculate(FormulaType formulaType, Stock stock, int period1, int period2) {

        float sum = 0;
        float first_MA = 0;
        float second_MA = 0;

        switch (formulaType) {

            case LAST_TRADE:
                float[] ret1 = {stock.getLastTrade()};
                return ret1;

            case LIN_MOVING_AVERAGE:

                for(int x = 0; x<period1; x=x+1) {
                    sum = sum + stock.getHistory().get(x).getClose();
                }
                sum = sum/period1;
                first_MA = sum;

                sum = 0;

                for(int x = 0; x<period2; x=x+1) {
                    sum = sum + stock.getHistory().get(x).getClose();
                }
                sum = sum/period2;
                second_MA = sum;

                float[] ret2 = {first_MA, second_MA};
                return ret2;

            case EXP_MOVING_AVERAGE:

                float SMA1 = 0;
                float SMA2 = 0;

                for(int x = 0; x<period1; x=x+1) {
                    sum = sum + stock.getHistory().get(x).getClose();
                }
                SMA1 = sum/period1;

                sum = 0;

                for(int x = 0; x<period2; x=x+1) {
                    sum = sum + stock.getHistory().get(x).getClose();
                }
                SMA2 = sum/period2;

                float multiplier1 = 2/(period1+1);
                float multiplier2 = 2/(period2+1);

                first_MA = (stock.getLastTrade()-SMA1)*multiplier1+SMA1;
                second_MA = (stock.getLastTrade()-SMA2)*multiplier2+SMA2;

                float[] ret3 = {first_MA,second_MA};
                return ret3;

            case CORRELATION:
                break;
            default:
                float[] ret4 = {0};
                return ret4;

        }

        float[] ret4 = {0};
        return ret4;

    }

    public static float[] calculate(FormulaType formulaType, Stock stock1, Stock stock2, int period) {

        ArrayList<HistoryPoint> history1 = stock1.getHistory();
        ArrayList<HistoryPoint> history2 = stock2.getHistory();

        switch (formulaType) {
            case CORRELATION:

                float sum = 0;
                float average1 = 0;
                float average2 = 0;
                float sumSQprices = 0;
                float sumSQprice1 = 0;
                float est_a1 = 0;
                float est_a0 = 0;
                List<Float> array_residuals = new ArrayList<Float>();
                float SCR = 0;
                float elem = 0;
                double variance_res = 0;
                double variance_a1 = 0;
                float student_ratio = 0;
                float SCT = 0;
                float R2 = 0;

                // Average of stock1 prices
                for(int x = 0; x<period; x=x+1) {
                    sum = sum + history1.get(x).getClose();
                }
                average1 = sum/period;
                sum = 0;

                // Average of stock2 prices
                for(int x = 0; x<period; x=x+1) {
                    sum = sum + history2.get(x).getClose();
                }
                average2 = sum/period;
                sum = 0;

                // Sum of price1 * price2
                for(int x=0; x<period; x=x+1){
                    sum = sum + history1.get(x).getClose() * history2.get(x).getClose();
                }
                sumSQprices = sum;
                sum = 0;

                // Sum of square of price1
                for(int x=0; x<period; x=x+1){
                    sum = sum + history1.get(x).getClose() * history1.get(x).getClose();
                }
                sumSQprice1 = sum;
                sum = 0;

                // Calculating the estimation of a1 coefficient
                est_a1 = (sumSQprices - period * average1 * average2) / (sumSQprice1 - period * average1 * average1);

                // Calculating the estimation of a0 coefficient
                est_a0 = average2 - est_a1 * average1;

                // Calculating the array of residuals and the SCR (somme des carrés des résidus)
                for(int x=0; x<period; x=x+1){
                    elem = history2.get(x).getClose() - est_a0 - est_a1 * history1.get(x).getClose();
                    array_residuals.add(elem);
                    sum = sum + elem * elem;
                }
                SCR = sum;
                sum = 0;

                // Calculating the estimation of the variances
                variance_res = SCR/(period - 2);
                for(int x=0; x<period; x=x+1){
                    sum = sum + (history1.get(x).getClose() - average1) *
                            (history1.get(x).getClose() - average1);
                }
                variance_a1 = variance_res/sum;
                sum = 0;

                // Calculating the ratio of Student
                student_ratio = (float) (est_a1/Math.sqrt(variance_a1));

                // Calculating SCT and R2
                for(int x=0; x<period; x=x+1){
                    sum = sum + (history2.get(x).getClose() - average2) *
                            (history2.get(x).getClose() - average2);
                }
                SCT = sum;
                sum = 0;
                R2 = 1 - SCR/SCT;

                float[] ret5 = {R2,student_ratio};
                return ret5;

            default:
                float[] ret6 = {0};
                return ret6;
        }

    }

}
